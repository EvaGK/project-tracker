const express= require("express")
const users = express.Router()
const db=require('../DB/dbConn').dataPool
const bcrypt = require('bcrypt')

// Registracija
users.post('/register', async (req, res)=>{
  let { username, email, password } = req.body;

  console.log("Received registration data:", req.body); // Add this line

  if (!username || !email || !password) {
      return res.status(400).json({ message: 'All fields required' });
  }

  try {
      // Hash funkcija
      const hashedPassword = await bcrypt.hash(password, 10);

      // Doda uporabnika v db
      await db.AddUser(username, email, hashedPassword);

      console.log("User registered successfully");
      res.status(201).json({ message: 'Registration successful' });
  } catch (error) {
      console.error("Error during registration:", error);
      res.status(500).json({ message: 'Server error' });
  }
  })

// Login
users.post('/login', async (req, res) => {
  const { username, password } = req.body;

  if (!username || !password) {
      return res.status(400).json({ message: 'All fields required' });
  }

  try {
      // Dobimo uporabnika iz db
      const user = await db.AuthUser(username);

      if (!user.length) {
          return res.status(400).json({ message: 'Username and password do not match' });
      }

      // Primerja geslo
      const isMatch = await bcrypt.compare(password, user[0].user_password);

      if (!isMatch) {
          return res.status(400).json({ message: 'Username and password do not match' });
      }

      res.status(200).json({ message: 'Login successful' });
  } catch (error) {
      console.error("Error during login:", error);
      res.status(500).json({ message: 'Server error' });
  }
});

  module.exports = users