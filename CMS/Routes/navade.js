const express=require("express")
const navade=express.Router()
const db=require("../DB/dbConn")

//dobimo vse navade za uporabnika
navade.get('/:user_id', async (req, res)=>{
    try{
        const user_id = req.params.user_id
        var queryResult=await db.getHabits(user_id)
        res.json(queryResult)
    }
    catch(err){
        console.log(err)
        res.status(500).json({message: 'Error fetching habits'})
    }
})

//ustvari novo navado
navade.post('/', async (req, res)=>{
    const{name, description, user_id}=req.body

    if (!name || !user_id){
        return res.status(400).json({message: 'Name and user_id required'})
    }
    try {
        const queryResult = await db.createHabit(name, description, user_id)
        res.status(201).json({ message: 'Habit created', habit_id: queryResult.insertId })
    } catch (err){
        console.log(err)
        res.status(500).json({message: 'Error creating habit'})
    }
})

//updejtamo streak navade
navade.put('/streak/:habit_id', async (req, res)=>{
    const {habit_id} = req.params.habit_id
    const {streak} = req.body

    if (streak == null){
        return res.status(400).json({message: 'Streak required '})
    }
    try {
        await db.updateHabitStreak(habit_id, streak)
        res.json({message: 'Habit streak updated'})
    } catch (err){
        console.log(err)
        res.status(500).json({message: 'Error updating habit streak'})
    }
})

//Updejtamo description navade
navade.put('/description/:habit_id', async (req, res)=>{
    const {habit_id} = req.params.habit_id
    const {description} = req.body

    if (!description){
        return res.status(400).json({message: 'Description required '})
    }
    try {
        await db.updateHabitDescription(habit_id, description)
        res.json({message: 'Habit description updated'})
    } catch (err){
        console.log(err)
        res.status(500).json({message: 'Error updating habit description'})
    }
})

//izbrisemo navado
navade.delete('/:habit_id', async (req, res)=>{
    const habit_id = req.params.habit_id

    try {
        await db.deleteHabit(habit_id)
        res.json({message: 'Habit deleted successfully'})
    }catch(err){
        console.log(err)
        res.status(500).json({message: 'Error deleting habit'})
    }
})

module.exports=navade
    