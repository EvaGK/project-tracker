const express = require("express")
const posts = express.Router()
const db = require('../DB/dbConn').dataPool

//Dobimo vse objave
posts.get('/', async (req, res)=>{
    try {
        const queryResult = await db.getPosts()
        res.json(queryResult)
    } catch (err){
        console.log(err)
        res.status(500).json({message: 'Error fetching posts'})
    }
})

//Ustvari objavo
posts.post('/', async (req, res, next)=>{
    let title = req.body.title
    let content = req.body.content
    let user_id = 1
    let upvotes = 0

    if (!title || !content){
        return res.status(400).json({message: 'Title and content required'})
    }
    try {
        var queryResult = await db.createPost(title, content)
        
    } catch (err){
        console.log(err)
        res.status(500).json({message: 'Error creating post'})
    }
})

//Updejtamo stevilo lajkov
posts.put('/upvotes', async (req, res)=>{
    const post_id = req.params.post_id;
    const { upvotes } = req.body;

    if (upvotes == null){
        return res.status(400).json({message: 'Upvotes required'})
    }
    try {
        await db.updatePostUpvotes(post_id, upvotes)
        res.json({message: 'Upvotes updated'})
    } catch (err){
        console.log(err)
        res.status(500).json({message: 'Error updating upvotes'})
    }
})

posts.delete('/:post_id', async (req, res)=>{
    const post_id = req.params.post_id

    try {
        await db.deletePost(post_id)
        res.json({message: 'Post deleted successfully'})
    } catch (err){
        console.log(err)
        res.status(500).json({message: 'Error deleting post'})
    }
})

module.exports = posts