const mysql = require('mysql2')
const dotenv = require("dotenv")
dotenv.config()
const express = require('express')

console.log("DB Host:", process.env.DB_HOST);
console.log("DB User:", process.env.DB_USER);
console.log("DB Password:", process.env.DB_PASS);
console.log("DB Name:", process.env.DB_NAME);

const conn = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.DB_NAME,
});

conn.connect((err) => {
  if (err) {
      console.log("ERROR: " + err.message);
      return;
  }
  console.log('Connection established');
});

let dataPool = {};

dataPool.getHabits = (user_id) => {
  return new Promise((resolve, reject) => {
      conn.query(`SELECT * FROM habits WHERE user_id = ?`, [user_id], (err, res) => {
          if (err) { return reject(err); }
          return resolve(res);
      });
  });
};

dataPool.createHabit = (name, description, user_id) => {
  return new Promise((resolve, reject) => {
      conn.query(`INSERT INTO habits (name, description, user_id) VALUES (?, ?, ?)`, [name, description, user_id], (err, res) => {
          if (err) { return reject(err); }
          return resolve(res);
      });
  });
};

dataPool.updateHabitStreak = (habit_id, streak) => {
  return new Promise((resolve, reject) => {
      conn.query(`UPDATE habits SET streak = ? WHERE habit_id = ?`, [streak, habit_id], (err, res) => {
          if (err) { return reject(err); }
          return resolve(res);
      });
  });
};

dataPool.updateHabitDescription = (habit_id, description) => {
  return new Promise((resolve, reject) => {
      conn.query(`UPDATE habits SET description = ? WHERE habit_id = ?`, [description, habit_id], (err, res) => {
          if (err) { return reject(err); }
          return resolve(res);
      });
  });
};

dataPool.deleteHabit = (habit_id) => {
  return new Promise((resolve, reject) => {
      conn.query(`DELETE FROM habits WHERE habit_id = ?`, [habit_id], (err, res) => {
          if (err) { return reject(err); }
          return resolve(res);
      });
  });
};

//Dobimo vse objave
dataPool.getPosts = ()=>{
  return new Promise((resolve, reject) => {
      conn.query('SELECT * FROM posts', (err, res)=>{
          if (err){
              return reject(err)
          }
          return resolve(res)
      })
  })
}

//Nova objava
dataPool.createPost = (title, content) => {
  const user_id = 2;
  return new Promise((resolve, reject) => {
    conn.query(
      'INSERT INTO posts (title, content, upvotes, user_id) VALUES (?, ?, 0, ?)',
      [title, content, user_id],
      (err, res) => {
        if (err) {
          return reject(err);
        }
        return resolve(res);
      }
    );
  });
};


//spremeni lajke
dataPool.updatePostUpvotes = (post_id, upvotes)=>{
  return new Promise((resolve, reject)=>{
      conn.query('UPDATE posts SET upvotes = ? WHERE id = ?', [upvotes, post_id], (err, res)=>{
          if (err) {
              return reject(err)
          }
          return resolve(res)
      })
  })
}

//Izbris objave
dataPool.deletePost = (post_id)=>{
  return new Promise((resolve, reject)=>{
      conn.query('DELETE FROM posts WHERE id = ?', [post_id], (err, res)=>{
          if (err) {
              return reject(err)
          }
          return resolve(res)
      })
  })
}



// preverjanje in dodajanje uporabnikov
dataPool.AuthUser = (username) => {
  return new Promise((resolve, reject) => {
      conn.query('SELECT * FROM users WHERE user_name = ?', username, (err, res) => {
          if (err) {
              console.log("Error fetching user:", err);
              return reject(err);
          }
          console.log("User fetched successfully:", res);
          return resolve(res);
      });
  });
};

dataPool.AddUser = (username, email, password) => {
  return new Promise((resolve, reject) => {
      console.log("Adding user to database:", username, email);
      conn.query('INSERT INTO users (user_name, user_email, user_password) VALUES (?, ?, ?)', [username, email, password], (err, res) => {
          if (err) {
              console.log("Error adding user:", err); // Add this line
              return reject(err);
          }
          console.log("User added to database"); // Add this line
          return resolve(res);
      });
  });
};

module.exports = { conn, dataPool };