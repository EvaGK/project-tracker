import { Component } from "react";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import HomeView from "./CustomComponents/HomeView";
import SignUpView from "./CustomComponents/SignUpView";
import LoginView from "./CustomComponents/LoginView";
import ForumView from "./CustomComponents/ForumView";
import HabitsView from "./CustomComponents/HabitsView";
import AddPostView from "./CustomComponents/AddPostView";
import axios from 'axios';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPage: "none",
      postID: 0,
    };
  }

  QSetView = (obj) => {
    this.setState({
      currentPage: obj.page,
      postID: obj.id || 0,
    });
  };

  QGetView = () => {
    let page = this.state.currentPage;
    switch (page) {
      case "forum":
        return <ForumView QIDFromChild={this.QSetView} />;
      case "signup":
        return <SignUpView />;
      case "login":
        return <LoginView QUserFromChild={this.QHandleUserLog} />;
      case "habits":
        return <HabitsView/>;
      case "addpost":
        return <AddPostView/>
      default:
        return <HomeView />;
    }
  };

  QHandleUserLog = (user) => {
    axios.post('http://localhost:5059/users/login', {
      username: user.username,
      password: user.password
    })
    .then(response => {
      console.log('Login successful');
      toast.success('Login successful');
      this.QSetView({ page: 'home' });
    })
    .catch(error => {
      console.error('Login failed:', error);
      toast.error('Login failed');
    });
  };

  render() {
    return (
      <div id="APP" className="container">
        <div id="menu" className="row">
          <nav className="navbar navbar-expand-lg navbar-dark bg-warning">
            <div className="container-fluid">
              <a
                onClick={() => this.QSetView({ page: "home" })}
                className="navbar-brand"
                href="/home"
              >
                <h1>Habit Tracker</h1>
              </a>
            </div>
            <ul className="nav nav-underline">
              <li className="nav-item">
                <button
                  onClick={() => this.QSetView({ page: "home" })}
                  className="nav-link active"
                >
                  Home
                </button>
              </li>

              <li className="nav-item">
                <button 
                onClick={() => this.QSetView({ page: "habits" })}
                className="nav-link">
                  Habits
                </button>
              </li>

              <li className="nav-item">
                <button
                  onClick={() => this.QSetView({ page: "forum" })}
                  className="nav-link"
                >
                  Forum
                </button>
              </li>

              <li className="nav-item">
                <button
                  onClick={() => this.QSetView({ page: "addpost" })}
                  className="nav-link"
                >
                  Write a post
                </button>
              </li>

              <li className="nav-item">
                <button
                  onClick={() => this.QSetView({ page: "signup" })}
                  className="nav-link"
                >
                  Sign up
                </button>
              </li>

              <li className="nav-item">
                <button
                  onClick={() => this.QSetView({ page: "login" })}
                  className="nav-link"
                >
                  Login
                </button>
              </li>
            </ul>
          </nav>

          <p className="text-center"></p>
        </div>

        <div id="viewer" className="row container">
          {this.QGetView(this.state)}
        </div>
        <ToastContainer />
      </div>
    );
  }
}

export default App;
