import { Component } from "react";
import axios from "axios";

class ForumView extends Component {
  constructor(props){
    super(props)
    this.state={
      Posts:[]
    }
  }

  QSetViewInParent = (obj) => {
    this.props.QIDFromChild(obj);
  };
  
  componentDidMount() {
  axios.get("http://localhost:5059/posts")
    .then(res => {
      this.setState({ Posts: res.data });
    })
    .catch(error => {
      console.error('Error fetching data:', error);
    });
}

// handleUpvote = (post_id) => {
//   const updatedUpvotes = this.state.Posts.find(post => post.id === post_id).upvotes + 1;
//   axios.put(`http://localhost:5059/posts/${post_id}`, { upvotes: updatedUpvotes })
//     .then(res => {
//       this.setState(prevState => ({
//         Posts: prevState.Posts.map(post => 
//           post.id === post_id ? { ...post, upvotes: updatedUpvotes } : post
//         )
//       }));
//     })
//     .catch(error => {
//       console.error('Error upvoting the post:', error);
//     });
// }

handleUpvote = (postId) => {
  // Find the post by postId and increment its upvotes count
  this.setState((prevState) => ({
    Posts: prevState.Posts.map((post) =>
      post.id === postId ? { ...post, upvotes: post.upvotes + 1 } : post
    )
  }));
};

handleDelete = (postId) => {
  axios.delete(`http://localhost:5059/posts/${postId}`)
    .then(res => {
      this.setState(prevState => ({
        Posts: prevState.Posts.filter(post => post.id !== postId)
      }));
      console.log("Post deleted")
    })
    .catch(error => {
      console.error('Error deleting the post:', error);
    });
}

render() {
  const { Posts } = this.state;
  return (
    <div className="card w-75 mb-3">
      {Posts.length > 0 ?
        Posts.map(post => (
          <div key={post.id} className="card-body">
            <h5 className="card-title">{post.title}</h5>
            <p className="card-text">{post.content}</p>
            <button
              onClick={() => this.handleUpvote(post.id)}
              style={{ margin: "10px" }}
              className="btn btn-primary"
            >
              Upvote ({post.upvotes || 0})
            </button>
            <button
              onClick={() => this.handleDelete(post.id)}
              style={{ margin: "10px" }}
              className="btn btn-danger"
            >
              Delete
            </button>
          </div>
        ))
        :
        "Loading..."
      }
    </div>
  );
}
}

export default ForumView;
