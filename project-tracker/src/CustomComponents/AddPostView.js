import { Component } from "react";
import axios from "axios";

class AddPostView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      post: {
        title: '',
        content: ''
      }
    };
  }
  
    
      QGetTextFromField = (e) => {
        this.setState((prevState) => ({
          post: { ...prevState.post, [e.target.name]: e.target.value },
        }));
      };
      QAddPost=()=>{
        axios.post("http://localhost:5059/posts",{
            title: this.state.post.title,
            content: this.state.post.content
        }).then(res=>{
            console.log("Sent to server...")
        }).catch(err=>{
          console.log("Error posting data:", err.message);
        })
      }
    render(){
        return(
            <div className="card" style={{margin:"10px"}}>
  <h3 style={{margin:"10px"}}>Welcome user</h3>
  <div className="mb-3" style={{margin:"10px"}}>
    <label  className="form-label">Title</label>
    <input 
    name="title"
    onChange={(e)=>this.QGetTextFromField(e)} 
    type="text" class="form-control"  
    placeholder="Title"/>
  </div>
  <div class="mb-3" style={{margin:"10px"}}>
    <label  class="form-label">Content</label>
    <textarea 
    name="content"
    onChange={(e)=>this.QGetTextFromField(e)} class="form-control" rows="3"></textarea>
  </div>
  <button  
  onClick={()=>this.QAddPost()}
  className="btn btn-primary bt" style={{margin:"10px"}}>Send</button>
</div>

        )
    }
}

export default AddPostView