import { Component } from "react";

class HomeView extends Component {
  render() {
    return (
      <div className="text">
        <p class="text-center">
          <h3>Welcome to the Habit Tracker!</h3>
        </p>
        <p class="text-center">
          This app allows users to track their daily habits and to write posts
          on the forum.
        </p>
        <p class="text-center">Start organizing your life:)</p>
      </div>
    );
  }
}

export default HomeView;
