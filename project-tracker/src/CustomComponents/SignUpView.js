import { Component } from "react";
import axios from "axios";

class SignUpView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {
        username: "",
        email: "",
        password: "",
      },
    };
  }

  QGetTextFromField = (e) => {
    this.setState((prevState) => ({
      user: { ...prevState.user, [e.target.name]: e.target.value },
    }));
  };

  QPostSignUp = (e) => {
    e.preventDefault();
    let user = this.state.user;
    console.log("User data being sent:", user);
    axios
      .post("http://localhost:5059/users/register", {
        username: user.username,
        email: user.email,
        password: user.password,
      })
      .then((res) => {
        console.log("Sent to server...", res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  

  render() {
    return (
      <div
        className="card"
        style={{
          width: "400px",
          marginLeft: "auto",
          marginRight: "auto",
          marginTop: "10px",
          marginBottom: "10px",
        }}
      >
        <form style={{ margin: "20px" }} onSubmit={this.QPostSignUp}>
          <div className="mb-3">
            <label className="form-label">Username</label>
            <input
              onChange={this.QGetTextFromField}
              name="username"
              type="text"
              className="form-control"
              id="username"
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Email</label>
            <input
              onChange={this.QGetTextFromField}
              name="email"
              type="email"
              className="form-control"
              id="email"
            />
          </div>
          <div className="mb-3">
            <label className="form-label">Password</label>
            <input
              onChange={this.QGetTextFromField}
              name="password"
              type="password"
              className="form-control"
              id="password"
            />
          </div>
          <button
            type="submit"
            style={{ margin: "10px" }}
            className="btn btn-primary"
          >
            Sign up
          </button>
        </form>
      </div>
    );
  }
}

export default SignUpView;
