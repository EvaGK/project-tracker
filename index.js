const express = require("express")
const app = express()
require('dotenv').config();
const cors = require("cors")

const port = process.env.PORT || 5059

const navade= require("./CMS/Routes/navade")
const users = require("./CMS/Routes/users")
const posts = require("./CMS/Routes/posts")

app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors({
    origin:["http://localhost:3059"],
    methods: ["GET", "POST", "DELETE", "PUT"],
    credentials:true
}))

//poskus
app.get('/', (req, res)=>{
    res.send('Habit Tracker')
})

app.use('/navade', navade)
app.use('/users', users)
app.use('/posts', posts)

const db = require("./CMS/DB/dbConn")

app.listen(process.env.PORT || port, ()=>{
    console.log(`Server port: ${process.env.PORT || port}`)
})
